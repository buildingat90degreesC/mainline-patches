#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

static uint32_t devmem_read(uint64_t addr)
{
    int fd = open("/dev/mem", O_RDONLY);
    char* map = mmap(0, 8192, PROT_READ, MAP_SHARED, fd, addr&-4096ull);
    uint32_t ans = *(uint32_t*)(map+(addr&4095));
    munmap(map, 8192);
    close(fd);
    if(getenv("MEM_DUMPS"))
        printf("{int}0x%zx = 0x%x\n", addr, ans);
    return ans;
}

#define GET32(a, b, c) devmem_read(a)
#define GET(a, b, c) ((GET32(a, b, c) >> b) & ((1u << c) - 1))
#define BIT(a, b, c) GET(a, b, 1)
#define ADDR(a, b, c) a

static uint64_t CLK_NULL(void)
{
    return 1;
}

static int CLK_NULL_get_src(void)
{
    return 1;
}

#define DECLARE_clk_mux(name)\
static int name ## _get_src(void);

#define DEFINE_clk_unimplemented(name)\
static uint64_t name(void)\
{\
    printf("%s: unimplemented\n", #name);\
    return 0;\
}

#define DEFINE_clk_fixed_rate(name, parent, rate, gate)\
static uint64_t name(void)\
{\
    if(!(parent)())\
    {\
        printf("%s: parent not enabled\n", #name);\
        return 0;\
    }\
    if(!(gate)())\
    {\
        printf("%s: gate not enabled\n", #name);\
        return 0;\
    }\
    printf("%s: enabled, fixed rate %lu\n", #name, (uint64_t)rate);\
    return rate;\
}

#define DEFINE_clk_pll(name, main, status, version, nfreq, mux, ...)\
static uint64_t name(void)\
{\
    if((int32_t)GET32 main < 0)\
    {\
        uint32_t con0 = GET32 main;\
        printf("%s: debug: *(uint32_t*)%p = 0x%x\n", #name, (void*)ADDR main, con0);\
        uint32_t mdiv = (con0 >> 16) & 0x3ff;\
        uint32_t pdiv = (con0 >> 8) & 0x3f;\
        uint32_t sdiv = con0 & 7;\
        if(!pdiv)\
        {\
            printf("%s: pdiv is 0\n", #name);\
            return 0;\
        }\
        uint64_t rate = (26000000*(uint64_t)mdiv)/((uint64_t)pdiv<<sdiv);\
        int mux_src = (mux ## _get_src)();\
        if(mux_src != 1)\
        {\
            printf("%s: mux source is not set to 1 (is %d instead)\n", #name, mux_src);\
            return 0;\
        }\
        printf("%s: enabled, rate = %lu\n", #name, rate);\
        return rate;\
    }\
    else\
    {\
        printf("%s: not enabled\n", #name);\
        return 0;\
    }\
}

#define DEFINE_clk_pll_141xx(...) DEFINE_clk_pll(__VA_ARGS__)
#define DEFINE_clk_pll_1431x(name, ...) DEFINE_clk_unimplemented(name) //DEFINE_clk_pll(__VA_ARGS__)

#define DEFINE_clk_fixed_div(name, parent, coef, gate)\
static uint64_t name(void)\
{\
    if(!(gate)())\
    {\
        printf("%s: gate not enabled\n", #name);\
        return 0;\
    }\
    uint64_t parent_rate = (parent)();\
    if(!parent_rate)\
    {\
        printf("%s: parent not enabled\n", #name);\
        return 0;\
    }\
    printf("%s: enabled, rate = %lu/%d = %lu\n", #name, parent_rate, coef, parent_rate/coef);\
    return parent_rate/coef;\
}

#define ARRAY(...) {__VA_ARGS__}

#define DEFINE_clk_mux(name, ps, main, status, enable, gate)\
static int name ## _get_src(void)\
{\
    if(!(gate)())\
    {\
        printf("%s: gate not enabled\n", #name);\
        return -1;\
    }\
    if(!BIT enable)\
    {\
        printf("%s: enable bit not set\n", #name);\
        return -1;\
    }\
    uint64_t(*parents[])(void) = ARRAY ps;\
    uint32_t reg = GET status;\
    if(!reg || (reg & (reg - 1)))\
    {\
        printf("%s: status register NPOT: 0x%x\n", #name, reg);\
        return -1;\
    }\
    int which = -1;\
    while(reg)\
    {\
        which++;\
        reg /= 2;\
    }\
    if(which * sizeof(*parents) >= sizeof(parents))\
    {\
        printf("%s: source out of range: %d\n", #name, which);\
        return -1;\
    }\
    printf("%s: using source #%d\n", #name, which);\
    return which;\
}\
static uint64_t name(void)\
{\
    int which = name ## _get_src();\
    if(which < 0)\
        return 0;\
    uint64_t(*parents[])(void) = ARRAY ps;\
    uint64_t parent_rate = 0;\
    _Pragma("GCC unroll (sizeof(parents)/sizeof(*parents))")\
    for(int i = 0; i * sizeof(*parents) < sizeof(parents); i++)\
        if(i == which)\
            parent_rate = parents[which]();\
    if(parent_rate)\
    {\
        printf("%s: source is enabled, rate = %lu\n", #name, parent_rate);\
        return parent_rate;\
    }\
    else\
    {\
        printf("%s: source is not enabled\n", #name);\
        return 0;\
    }\
}

#define DEFINE_clk_div(name, parent, main, status, gate)\
static uint64_t name(void)\
{\
    if(!(gate)())\
    {\
        printf("%s: gate not enabled\n", #name);\
        return 0;\
    }\
    uint64_t parent_rate = (parent)();\
    if(!parent_rate)\
    {\
        printf("%s: parent not enabled\n", #name);\
        return 0;\
    }\
    int coef = GET main + 1;\
    printf("%s: enabled, rate = %lu / %d = %lu\n", #name, parent_rate, coef, parent_rate/coef);\
    return parent_rate/coef;\
}

#define DEFINE_clk_gate(name, parent, reg)\
static uint64_t name(void)\
{\
    if(!BIT reg)\
    {\
        printf("%s: gate closed\n", #name);\
        return 0;\
    }\
    uint64_t parent_rate = (parent)();\
    if(!parent_rate)\
    {\
        printf("%s: parent not enabled\n", #name);\
        return 0;\
    }\
    printf("%s: enabled, rate = %lu\n", #name, parent_rate);\
    return parent_rate;\
}

static uint64_t VCLK_NULL(void)
{
    return 1;
}

#define DEFINE_vclk_unimplemented(name)\
static uint64_t name(void)\
{\
    printf("%s: unimplemented\n", #name);\
    return 0;\
}

DEFINE_vclk_unimplemented(VCLK_NYI)

#define DEFINE_d1(name, parent, clk)\
static uint64_t name(void)\
{\
    if(!(parent)())\
    {\
        printf("%s: parent not enabled\n", #name);\
        return 0;\
    }\
    uint64_t clk_rate = (clk)();\
    if(!clk_rate)\
    {\
        printf("%s: clock not enabled\n", #name);\
        return 0;\
    }\
    printf("%s: enabled, rate = %lu\n", #name, clk_rate);\
    return clk_rate;\
}

#define DEFINE_p1(...) DEFINE_d1(__VA_ARGS__)
#define DEFINE_umux(...) DEFINE_d1(__VA_ARGS__)

#define DEFINE_grpgate(name, parent, gs)\
static uint64_t name(void)\
{\
    if(!(parent)())\
    {\
        printf("%s: parent not enabled\n", #name);\
        return 0;\
    }\
    uint64_t(*gates[])(void) = ARRAY gs;\
    uint64_t gate0_rate = 1;\
    _Pragma("GCC unroll (sizeof(gates)/sizeof(*gates))")\
    for(int i = 0; i * sizeof(*gates) < sizeof(gates); i++)\
    {\
        printf("%s: checking gate #%d...\n", #name, i);\
        uint64_t rate = gates[i]();\
        if(!i)\
            gate0_rate = rate;\
        if(!rate)\
        {\
            printf("%s: gate #%d not enabled\n", #name, i);\
            return 0;\
        }\
    }\
    printf("%s: enabled, rate = %lu\n", #name, gate0_rate);\
    return gate0_rate;\
}

#define DEFINE_m1d1g1(name, parent, mux, div, gate, extmux)\
static uint64_t name(void)\
{\
    if(!(parent)())\
    {\
        printf("%s: parent not enabled\n", #name);\
        return 0;\
    }\
    uint64_t mux_rate = (mux)();\
    if(!mux_rate)\
    {\
        printf("%s: mux not enabled\n", #name);\
        return 0;\
    }\
    printf("%s: mux rate is %lu\n", #name, mux_rate);\
    uint64_t div_rate = (div)();\
    if(!div_rate)\
    {\
        printf("%s: div not enabled\n", #name);\
        return 0;\
    }\
    printf("%s: div rate is %lu\n", #name, div_rate);\
    uint64_t gate_rate = (gate)();\
    if(!gate_rate)\
    {\
        printf("%s: gate not enabled\n", #name);\
        return 0;\
    }\
    printf("%s: gate rate is %lu\n", #name, gate_rate);\
    if(!(extmux)())\
    {\
        printf("%s: extmux not enabled\n", #name);\
        return 0;\
    }\
    uint64_t rate = 0;\
    if((gate) != CLK_NULL)\
        rate = gate_rate;\
    else if((div) != CLK_NULL)\
        rate = div_rate;\
    else if((mux) != CLK_NULL)\
        rate = mux_rate;\
    printf("%s: enabled, rate = %lu\n", #name, rate);\
    return rate;\
}

#define DEFINE_pxmxdx(name, ...) DEFINE_vclk_unimplemented(name)

#define VCLK_TABLE(...) uint64_t(*const clocks[])(void) = __VA_ARGS__;

#include "data.txt"

int main(int argc, const char** argv)
{
    int nr = 0;
    if(!strcmp(argv[1], "all"))
    {
        for(int i = 0; i < sizeof(clocks) / sizeof(*clocks); i++)
            if(clocks[i])
                printf("clocks[%d]() = %ld\n", i, clocks[i]());
    }
    else
    {
        sscanf(argv[1], "%x", &nr);
        printf("clocks[%d]() = %ld\n", nr, clocks[nr]());
    }
    return 0;
}
