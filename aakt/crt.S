.global _start
.global reset
.global is_excsvc
.global smc_call

_start:
mrs x0, currentel
cmp x0, #8
beq hyp_setup_from_hyp
ldr x0, =0xc2000400
adr x1, hyp_setup
ldr x2, =4096
ldr x3, =1
ldr x4, =123
ldr x5, =123
smc #0
hvc #0
adr x0, excsvc
msr vbar_el1, x0
ret

hyp_setup_from_hyp:
mov x1, xzr
mrs x0, hcr_el2
orr x0, x0, #0x80000000
msr hcr_el2, x0
adr x0, rabbit
msr elr_el2, x0
ldr x0, =4
msr spsr_el2, x0

hyp_setup:
adr x0, stacks+4096
mov sp, x0
str x1, [sp, #-16]!
// set exception vectors
adr x0, exctbl
msr vbar_el2, x0
// other setup goes in c code
// return
ldr x1, [sp], #16
tst x1, x1
beq do_eret
ldr x0, =0xc2000401
mov x1, xzr
smc #0

.p2align 16
stacks:
.long 0
.p2align 16

exctbl:
b exc
.p2align 7
b exc
.p2align 7
b exc
.p2align 7
b exc
.p2align 7
b exc
.p2align 7
b exc
.p2align 7
b exc
.p2align 7
b exc
.p2align 7
b exc
.p2align 7
b exc
.p2align 7
b exc
.p2align 7
b exc
.p2align 7
b exc
.p2align 7
b exc
.p2align 7
b exc
.p2align 7
b exc
.p2align 7

.p2align 11
excsvc:
hvc #0
.p2align 7
hvc #0
.p2align 7
hvc #0
.p2align 7
hvc #0
.p2align 7
hvc #0
.p2align 7
hvc #0
.p2align 7
hvc #0
.p2align 7
hvc #0
.p2align 7
hvc #0
.p2align 7
hvc #0
.p2align 7
hvc #0
.p2align 7
hvc #0
.p2align 7
hvc #0
.p2align 7
hvc #0
.p2align 7
hvc #0
.p2align 7
hvc #0
.p2align 7
excsvc_end:

exc:
str x30, [sp, #-16]!
str x29, [sp, #-16]!
str x28, [sp, #-16]!
str x27, [sp, #-16]!
str x26, [sp, #-16]!
str x25, [sp, #-16]!
str x24, [sp, #-16]!
str x23, [sp, #-16]!
str x22, [sp, #-16]!
str x21, [sp, #-16]!
str x20, [sp, #-16]!
str x19, [sp, #-16]!
str x18, [sp, #-16]!
str x17, [sp, #-16]!
str x16, [sp, #-16]!
str x15, [sp, #-16]!
str x14, [sp, #-16]!
str x13, [sp, #-16]!
str x12, [sp, #-16]!
str x11, [sp, #-16]!
str x10, [sp, #-16]!
str x9, [sp, #-16]!
str x8, [sp, #-16]!
str x7, [sp, #-16]!
str x6, [sp, #-16]!
str x5, [sp, #-16]!
str x4, [sp, #-16]!
str x3, [sp, #-16]!
str x2, [sp, #-16]!
str x1, [sp, #-16]!
str x0, [sp, #-16]!
mov x0, sp
mrs x1, elr_el2
str x1, [x0, #8]
mrs x1, spsr_el2
str x1, [x0, #24]
mrs x1, esr_el2
str x1, [x0, #40]
mrs x1, mdcr_el2
str x1, [x0, #56]
mrs x1, mdscr_el1
str x1, [x0, #72]
mrs x1, dbgbcr0_el1
str x1, [x0, #88]
mrs x1, dbgbvr0_el1
str x1, [x0, #104]
mrs x1, id_aa64dfr0_el1
str x1, [x0, #120]
mrs x1, elr_el1
str x1, [x0, #136]
mrs x1, spsr_el1
str x1, [x0, #152]
mrs x1, esr_el1
str x1, [x0, #168]
mrs x1, far_el2
str x1, [x0, #184]
mrs x1, far_el1
str x1, [x0, #200]
bl entry
mov x0, sp
ldr x1, [x0, #104]
msr dbgbvr0_el1, x1
ldr x1, [x0, #88]
msr dbgbcr0_el1, x1
ldr x1, [x0, #72]
msr mdscr_el1, x1
ldr x1, [x0, #56]
msr mdcr_el2, x1
ldr x1, [x0, #24]
msr spsr_el2, x1
ldr x1, [x0, #8]
msr elr_el2, x1
ldr x0, [sp], #16
ldr x1, [sp], #16
ldr x2, [sp], #16
ldr x3, [sp], #16
ldr x4, [sp], #16
ldr x5, [sp], #16
ldr x6, [sp], #16
ldr x7, [sp], #16
ldr x8, [sp], #16
ldr x9, [sp], #16
ldr x10, [sp], #16
ldr x11, [sp], #16
ldr x12, [sp], #16
ldr x13, [sp], #16
ldr x14, [sp], #16
ldr x15, [sp], #16
ldr x16, [sp], #16
ldr x17, [sp], #16
ldr x18, [sp], #16
ldr x19, [sp], #16
ldr x20, [sp], #16
ldr x21, [sp], #16
ldr x22, [sp], #16
ldr x23, [sp], #16
ldr x24, [sp], #16
ldr x25, [sp], #16
ldr x26, [sp], #16
ldr x27, [sp], #16
ldr x28, [sp], #16
ldr x29, [sp], #16
ldr x30, [sp], #16
do_eret:
eret

rabbit:
hvc 0
ldr x0, =1
add x0, x0, x0
add x0, x0, x0
add x0, x0, x0
add x0, x0, x0
add x0, x0, x0
add x0, x0, x0
add x0, x0, x0
add x0, x0, x0
hang:
b hang

reset:
ldr x0, =0x84000009
mov x1, xzr
mov x2, xzr
mov x3, xzr
mov x4, xzr
mov x5, xzr
mov x6, xzr
mov x7, xzr
smc #0
ret

is_excsvc:
mov x2, xzr
adr x1, excsvc
cmp x0, x1
bcc not_excsvc
adr x1, excsvc_end
cmp x0, x1
bcs not_excsvc
add x2, x2, #1
not_excsvc:
mov x0, x2
ret

smc_call:
dsb sy
smc #0
ret
