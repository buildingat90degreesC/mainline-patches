import sys

while True:
    x = [sys.stdin.buffer.raw.read(1) for i in range(6)]
    if not sum(map(len, x)): break
    c = 0
    for bit in range(8):
        y = {}
        for i in x:
            if not i: continue
            i = i[0] & (1 << bit)
            if i in y: y[i] += 1
            else: y[i] = 1
        c |= next(i for i in y if y[i] == max(y.values()))
    sys.stdout.buffer.raw.write(bytes((c,)))
