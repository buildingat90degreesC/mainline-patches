/* CONSTANTS, TWEAK FOR YOUR SOC */

#define FB_ADDR 0x67000000
#define FB_WIDTH 720
#define FB_HEIGHT 1480
#define REBOOT_ADDR 0x10480400
#define REBOOT_VALUE 1
//assuming 32-bit color

#define RAMOOPS_END 0x46a00000
#define RAMOOPS_START 0x46800000

typedef unsigned long long uint64_t;
typedef signed long long int64_t;
typedef unsigned int uint32_t;

struct Register
{
    uint64_t value;
    uint64_t special;
};

static inline void reboot(void)
{
    *(uint32_t*)REBOOT_ADDR = REBOOT_VALUE;
}

void putchar0(char c)
{
    static char* cur = 0;
    static char* const end = (void*)RAMOOPS_END;
    if(!cur)
    {
        cur = (void*)RAMOOPS_START;
        for(char* i = cur; i < end; i++)
            *i = '#';
    }
    char* q = cur;
    if(q != end)
    {
        *q++ = c;
        cur = q;
    }
}

void putchar(char c)
{
    for(int i = 0; i < 6; i++)
        putchar0(c);
}

void writes(const char* s)
{
    while(*s)
        putchar(*s++);
}

void write_hex(uint64_t reg)
{
    for(int i = 60; i >= 0; i -= 4)
    {
        int q = (reg >> i) & 15;
        if(q < 10)
            putchar(q+'0');
        else
            putchar(q-10+'a');
    }
}

void reset(void);

int is_excsvc(uint64_t addr);

void test_framebuffer(void)
{
    volatile int* fb = (void*)FB_ADDR;
    for(int i = 0; i < FB_WIDTH * FB_HEIGHT; i++)
    {
        int row = (i / FB_WIDTH) / 16;
        int col = (i % FB_WIDTH) / 16;
        if((row ^ col) & 1)
            fb[i] = 0xffff0000;
        else
            fb[i] = 0xff00ffff;
    }
}

void kernel_panicked(void)
{
    volatile int* fb = (void*)FB_ADDR;
    for(int i = 0; i < FB_WIDTH * FB_HEIGHT; i++)
        fb[i] = 0xffff0000;
    for(;;)
        asm volatile("");
}

void verify_clock_enabled(void);

int had_string(char c, const char* s)
{
    static int prefix = 0;
    if(!s[prefix])
        return 1;
    while(prefix > 0 && s[prefix] != c)
    {
        for(int i = 1; i <= prefix; i++)
        {
            int ok = 1;
            for(int j = 0; ok && i + j < prefix; j++)
                if(s[j] != s[i+j])
                    ok = 0;
            if(ok)
            {
                prefix -= i;
                break;
            }
        }
    }
    if(s[prefix] == c)
        prefix++;
    return 0;
}

int smc_call(int a, int b, int c, int d);

void entry(struct Register* regs)
{
    static int first = 1;
    if(first)
    {
        first = 0;
        writes("aakt build on " BUILD_DATE "\n");
        test_framebuffer();
        /* SOC-SPECIFIC, NEED PROPER data.txt TO PROCEED */
        //verify_clock_enabled();
    }
    else if(regs[2].special == 0x5a000000)
    {
        if(regs[0].value == 0xdeaddead)
            kernel_panicked();
        putchar(regs[0].value);
        return;
    }
    if(regs[2].special == 0x5a000000)
        return;
    //below is some old debugging code. enable if you feel the need for it
    //note that we're running with caches disabled, memory may not be coherent
    writes("\n===========================================\npc  = ");
    write_hex(regs[0].special);
    writes("\ncpsr= ");
    write_hex(regs[1].special);
    writes("\nesr = ");
    write_hex(regs[2].special);
    writes("\nmdcr= ");
    write_hex(regs[3].special);
    writes("\nmscr= ");
    write_hex(regs[4].special);
    writes("\ndbgc= ");
    write_hex(regs[5].special);
    writes("\ndbgv= ");
    write_hex(regs[6].special);
    writes("\ndbgf= ");
    write_hex(regs[7].special);
    writes("\npc1 = ");
    write_hex(regs[8].special);
    writes("\ncps1= ");
    write_hex(regs[9].special);
    writes("\nesr1= ");
    write_hex(regs[10].special);
    writes("\nfar2= ");
    write_hex(regs[11].special);
    writes("\nfar1= ");
    write_hex(regs[12].special);
    for(int i = 0; i < 31; i++)
    {
        putchar('\n');
        putchar('x');
        if(i < 10)
        {
            putchar(i+'0');
            putchar(' ');
        }
        else
        {
            putchar((i / 10)+'0');
            putchar((i % 10)+'0');
        }
        putchar(' ');
        putchar('=');
        putchar(' ');
        write_hex(regs[i].value);
    }
    writes("\n===========================================\n");
    return;
    uint64_t ttbr0_el1;
    asm volatile("mrs %0, ttbr0_el1":"=r"(ttbr0_el1));
    writes("ttbr0_el1 = ");
    write_hex(ttbr0_el1);
    putchar('\n');
    uint64_t ttbr1_el1;
    asm volatile("mrs %0, ttbr1_el1":"=r"(ttbr1_el1));
    writes("ttbr1_el1 = ");
    write_hex(ttbr1_el1);
    putchar('\n');
    writes("{long}0x40270800 = ");
    write_hex(*(volatile uint64_t*)0x40270800);
    putchar('\n');
    if((int64_t)regs[0].special >= 0 && *(volatile uint32_t*)regs[0].special == 0xd61f0100)
    {
        uint64_t dst = regs[8].value;
        writes("ttbr0:\n");
        for(int i = 0; i < 4096; i += 8)
        {
            putchar('*');
            putchar(' ');
            write_hex(i);
            writes(" -> ");
            write_hex(*(volatile uint64_t*)(ttbr0_el1+i));
            putchar('\n');
        }
        writes("ttbr1:\n");
        for(int i = 0; i < 4096; i += 8)
        {
            putchar('*');
            putchar(' ');
            write_hex(i);
            writes(" -> ");
            write_hex(*(volatile uint64_t*)(ttbr1_el1+i));
            putchar('\n');
        }
        uint64_t cur = -4096ull & *(volatile uint64_t*)(ttbr1_el1 + (((dst >> 30) & 511) << 3));
        writes("resolve: ");
        write_hex(cur);
        putchar('\n');
        for(int i = 21; i >= 12 && cur; i -= 9)
        {
            cur = -4096ull & *(volatile uint64_t*)(cur + (((dst >> i) & 511) << 3));
            writes("resolve: ");
            write_hex(cur);
            putchar('\n');
        }
    }
    if(is_excsvc(regs[0].special))
    {
        writes("Note: exception taken in EL1\n");
        regs[0].special = regs[8].special;
        regs[1].special = regs[9].special;
    }
#if 0
    if((int64_t)regs[0].special >= 0 && (*(volatile uint32_t*)regs[0].special == 0xd5100241 || *(volatile uint32_t*)regs[0].special == 0xd5100240))
    {
        writes("Note: jumping over misbehaving instruction\n");
        regs[0].special += 4;
    }
    regs[1].special &= ~0x300ull; //D=0, A=0
    regs[1].special |= 0x200000; //SS=1
    regs[3].special |= 256; //TDE=1
    regs[4].special |= 0xa001; //MDE=1, KDE=1, SS=1
    regs[5].special = 0x1000ff;
    regs[6].special = regs[0].special + 4;
#endif
}
