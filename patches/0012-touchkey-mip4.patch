Input: add mip4 touchkey driver

Implement a driver for touchkey devices compatible with the MIP4 protocol.

Signed-off-by: methanal <baclofen@tuta.io>

diff --git a/Documentation/devicetree/bindings/input/melfas,mip4_touchkey.yaml b/Documentation/devicetree/bindings/input/melfas,mip4_touchkey.yaml
new file mode 100644
index 000000000000..82d8c7a310b8
--- /dev/null
+++ b/Documentation/devicetree/bindings/input/melfas,mip4_touchkey.yaml
@@ -0,0 +1,56 @@
+# SPDX-License-Identifier: (GPL-2.0-only OR BSD-2-Clause)
+%YAML 1.2
+---
+$id: http://devicetree.org/schemas/input/melfas,mip4_touchkey.yaml#
+$schema: http://devicetree.org/meta-schemas/core.yaml#
+
+title: MELFAS MIP4 compatible touchkey driver
+
+description: |
+  MIP4 (MELFAS Interface Protocol v4) is a protocol developed by MELFAS. This
+  driver implements support for MIP4 compatible touchkeys, such as the MHS
+  series chips by MELFAS.
+
+maintainers:
+  - methanal <baclofen@tuta.io>
+
+allOf:
+  - $ref: input.yaml#
+
+properties:
+  compatible:
+    const: melfas,mip4_touchkey
+
+  reg:
+    maxItems: 1
+
+  interrupts:
+    maxItems: 1
+
+  vdd-supply:
+    description: Power supply to the touchkey device
+
+  linux,keycodes:
+    description: Keycodes for the touchkeys
+
+additionalProperties: false
+
+required:
+  - compatible
+  - reg
+  - interrupts
+
+examples:
+  - |
+    #include <dt-bindings/input/input.h>
+    #include <dt-bindings/interrupt-controller/irq.h>
+    touchkey@49 {
+      compatible = "melfas,mip4_touchkey";
+      reg = <0x49>;
+      interrupt-parent = <&gpio>;
+      interrupts = <7 IRQ_TYPE_EDGE_FALLING>;
+      vdd-supply = <&ldo>;
+      linux,keycodes = <KEY_MENU KEY_BACK>;
+    };
+
+...
diff --git a/drivers/input/keyboard/Kconfig b/drivers/input/keyboard/Kconfig
index 1d0c5f4c0f99..0992042bf5b9 100644
--- a/drivers/input/keyboard/Kconfig
+++ b/drivers/input/keyboard/Kconfig
@@ -433,6 +433,17 @@ config KEYBOARD_MCS
 	  To compile this driver as a module, choose M here: the
 	  module will be called mcs_touchkey.
 
+config KEYBOARD_MIP4_TOUCHKEY
+	tristate "MELFAS MIP4 Touchkey"
+	depends on I2C
+	help
+	  Say Y here for MELFAS MIP4 compatible touchkey controller support.
+
+	  If unsure, say N.
+
+	  To compile this driver as a module, choose M here: the
+	  module will be called mip4_touchkey.
+
 config KEYBOARD_MPR121
 	tristate "Freescale MPR121 Touchkey"
 	depends on I2C
diff --git a/drivers/input/keyboard/Makefile b/drivers/input/keyboard/Makefile
index aecef00c5d09..d9b924a9b8c2 100644
--- a/drivers/input/keyboard/Makefile
+++ b/drivers/input/keyboard/Makefile
@@ -42,6 +42,7 @@ obj-$(CONFIG_KEYBOARD_MAPLE)		+= maple_keyb.o
 obj-$(CONFIG_KEYBOARD_MATRIX)		+= matrix_keypad.o
 obj-$(CONFIG_KEYBOARD_MAX7359)		+= max7359_keypad.o
 obj-$(CONFIG_KEYBOARD_MCS)		+= mcs_touchkey.o
+obj-$(CONFIG_KEYBOARD_MIP4_TOUCHKEY)	+= mip4_touchkey.o
 obj-$(CONFIG_KEYBOARD_MPR121)		+= mpr121_touchkey.o
 obj-$(CONFIG_KEYBOARD_MT6779)		+= mt6779-keypad.o
 obj-$(CONFIG_KEYBOARD_MTK_PMIC) 	+= mtk-pmic-keys.o
diff --git a/drivers/input/keyboard/mip4_touchkey.c b/drivers/input/keyboard/mip4_touchkey.c
new file mode 100644
index 000000000000..cbe10128c561
--- /dev/null
+++ b/drivers/input/keyboard/mip4_touchkey.c
@@ -0,0 +1,272 @@
+// SPDX-License-Identifier: GPL-2.0-or-later
+/*
+ * Copyright (C) 2016 MELFAS Inc.
+ *
+ * MELFAS MIP4 compatible driver for touchkeys.
+ */
+
+#include <linux/delay.h>
+#include <linux/i2c.h>
+#include <linux/input.h>
+#include <linux/interrupt.h>
+#include <linux/module.h>
+#include <linux/of.h>
+#include <linux/regulator/consumer.h>
+#include <linux/slab.h>
+
+#define MIP4_R0_INFO				0x01
+#define MIP4_R1_INFO_KEY_NUM			0x16
+#define MIP4_R1_INFO_IC_NAME			0x71
+
+#define MIP4_R0_EVENT				0x02
+#define MIP4_R1_EVENT_SIZE			0x06
+#define MIP4_R1_EVENT_PACKET_INFO		0x10
+#define MIP4_R1_EVENT_PACKET_DATA		0x11
+
+struct mip4_data {
+	struct i2c_client *client;
+	struct input_dev *input;
+	struct regulator *supply;
+
+	char ic[4];
+
+	u32 *keycodes;
+	u8 keycodes_count;
+
+	u8 *events;
+	u8 event_size;	
+};
+
+static int mip4_i2c_read(struct i2c_client *i2c, char *cmd, char *reply, u8 len)
+{
+	int error;
+	int retries = 3;
+	struct i2c_msg msg[] = {
+		{
+			.addr = i2c->addr,
+			.flags = 0,
+			.buf = cmd,
+			.len = 2,
+		}, {
+			.addr = i2c->addr,
+			.flags = I2C_M_RD,
+			.buf = reply,
+			.len = len,
+		},
+	};
+
+	while (retries) {
+		error = i2c_transfer(i2c->adapter, msg, ARRAY_SIZE(msg));
+		if (error == ARRAY_SIZE(msg))
+			return 0;
+
+		retries--;
+	}
+
+	dev_err(&i2c->dev, "Failed to read %d byte(s)\n", len);
+	return -EIO;
+}
+
+static irqreturn_t mip4_irq_func(int irq, void *ptr)
+{
+	struct mip4_data *data = ptr;
+	u8 cmd[2];
+	u8 events_count;
+	u8 keyidx;
+	u32 keycode;
+	bool status;
+	int error;
+	int i;
+
+	cmd[0] = MIP4_R0_EVENT;
+	cmd[1] = MIP4_R1_EVENT_PACKET_INFO;
+	error = mip4_i2c_read(data->client, cmd, &events_count, 1);
+	if (error)
+		return IRQ_HANDLED;
+
+	/* Check the alert bit. */
+	if (events_count & BIT(7))
+		return IRQ_HANDLED;
+
+	events_count &= GENMASK(6, 0);
+
+	cmd[0] = MIP4_R0_EVENT;
+	cmd[1] = MIP4_R1_EVENT_PACKET_DATA;
+	error = mip4_i2c_read(data->client, cmd, data->events, events_count);
+	if (error)
+		return IRQ_HANDLED;
+
+	for (i = 0; i < events_count; i += data->event_size) {
+		keyidx = data->events[i] & GENMASK(3, 0);
+		status = data->events[i] & BIT(7);
+
+		if (keyidx) {
+			keycode = data->keycodes[keyidx - 1];
+
+			input_event(data->input, EV_MSC, MSC_SCAN, keycode);
+			input_report_key(data->input, keycode, status);
+		}
+	}
+	
+	input_sync(data->input);
+
+	return IRQ_HANDLED;
+}
+
+static int mip4_read_chip(struct mip4_data *data)
+{
+	u8 cmd[2];
+	int error;
+
+	cmd[0] = MIP4_R0_INFO;
+	cmd[1] = MIP4_R1_INFO_IC_NAME;
+	error = mip4_i2c_read(data->client, cmd, data->ic, sizeof(data->ic));
+	if (error)
+		return error;
+
+	cmd[0] = MIP4_R0_INFO;
+	cmd[1] = MIP4_R1_INFO_KEY_NUM;
+	error = mip4_i2c_read(data->client, cmd, &data->keycodes_count, 1);
+	if (error)
+		return error;
+
+	cmd[0] = MIP4_R0_EVENT;
+	cmd[1] = MIP4_R1_EVENT_SIZE;
+	error = mip4_i2c_read(data->client, cmd, &data->event_size, 1);
+	if (error)
+		return error;
+
+	return 0;
+}
+
+static int mip4_probe(struct i2c_client *client)
+{
+	struct mip4_data *data;
+	struct input_dev *input;
+	int flags;
+	int error;
+	int i;
+
+	input = devm_input_allocate_device(&client->dev);
+	if (!input)
+		return -ENOMEM;
+
+	data = devm_kzalloc(&client->dev, sizeof(*data), GFP_KERNEL);
+	if (!data)
+		return -ENOMEM;
+
+	data->input = input;
+	data->client = client;
+	data->supply = devm_regulator_get(&client->dev, "vdd");
+
+	input_set_drvdata(input, data);
+	i2c_set_clientdata(client, data);
+	
+	error = regulator_enable(data->supply);
+	if (error)
+		return error;
+	
+	msleep(300);
+
+	error = mip4_read_chip(data);
+	if (error)
+		return error;
+
+	input->id.bustype = BUS_I2C;
+	input->name = devm_kasprintf(&client->dev, GFP_KERNEL, "MELFAS %s",
+				     data->ic);
+	input->phys = devm_kasprintf(&client->dev, GFP_KERNEL, "%s/input0",
+				     dev_name(&client->dev));
+
+	/* Allocate arrays for keycodes and touchkey events. */
+	data->keycodes = devm_kcalloc(&client->dev, sizeof(*data->keycodes),
+				      data->keycodes_count, GFP_KERNEL);
+	data->events = devm_kcalloc(&client->dev, sizeof(*data->events),
+				    data->keycodes_count * data->event_size,
+				    GFP_KERNEL);
+
+#ifdef CONFIG_OF
+	of_property_read_u32_array(client->dev.of_node, "linux,keycodes",
+				   data->keycodes, data->keycodes_count);
+#endif
+
+	input->keycode = data->keycodes;
+	input->keycodesize = sizeof(*data->keycodes);
+	input->keycodemax = data->keycodes_count;
+
+	for (i = 0; i < data->keycodes_count; i++)
+		input_set_capability(input, EV_KEY, data->keycodes[i]);
+
+	error = input_register_device(input);
+	if (error) {
+		dev_err(&client->dev, "Couldn't register input device\n");
+		return error;
+	}
+
+	flags = irq_get_trigger_type(client->irq) | IRQF_ONESHOT;
+	error = devm_request_threaded_irq(&client->dev, client->irq, NULL,
+					  mip4_irq_func, flags, "mip4_touchkey",
+					  data);
+	if (error) {
+		dev_err(&client->dev, "Couldn't request IRQ %d\n", client->irq);
+		return error;
+	}
+
+	return 0;
+}
+
+static int mip4_suspend(struct device *dev)
+{
+	struct i2c_client *client = to_i2c_client(dev);
+	struct mip4_data *data = i2c_get_clientdata(client);
+	int i;
+
+	for (i = 0; i < data->keycodes_count; i++)
+		input_report_key(data->input, data->keycodes[i], 0);
+
+	input_sync(data->input);
+	disable_irq(client->irq);
+	regulator_disable(data->supply);
+
+	return 0;
+}
+
+static int mip4_resume(struct device *dev)
+{
+	struct i2c_client *client = to_i2c_client(dev);
+	struct mip4_data *data = i2c_get_clientdata(client);
+	int error;
+
+	error = regulator_enable(data->supply);
+	if (error)
+		return error;
+
+	msleep(300);
+	enable_irq(client->irq);
+
+	return 0;
+}
+
+static DEFINE_SIMPLE_DEV_PM_OPS(mip4_pm_ops, mip4_suspend, mip4_resume);
+
+#ifdef CONFIG_OF
+static const struct of_device_id mip4_of_match[] = {
+	{ .compatible = "melfas,mip4_touchkey", },
+	{ },
+};
+MODULE_DEVICE_TABLE(of, mip4_of_match);
+#endif
+
+static struct i2c_driver mip4_driver = {
+	.probe = mip4_probe,
+	.driver = {
+		.name = "mip4_touchkey",
+		.of_match_table = of_match_ptr(mip4_of_match),
+		.pm = pm_sleep_ptr(&mip4_pm_ops),
+	},
+};
+module_i2c_driver(mip4_driver);
+
+MODULE_DESCRIPTION("MELFAS MIP4 compatible touchkey driver");
+MODULE_AUTHOR("methanal <baclofen@tuta.io>");
+MODULE_LICENSE("GPL");
