#include <linux/module.h>
#include <linux/miscdevice.h>
#include <linux/pinctrl/consumer.h>
#include <linux/clk.h>

struct ring_buffer
{
    uint32_t head_offset;
    uint32_t tail_offset;
    uint32_t start;
    uint32_t size;
};

struct packet_item
{
    struct packet_item* next;
    size_t size;
    char data[];
};

struct packet_list
{
    struct mutex mtx;
    struct packet_item* tail;
    struct packet_item** head;
    size_t room_left;
};

#define PACKET_LIST_IP ((struct packet_list*)1)

struct protocol_map
{
    uint16_t protocol;
    struct packet_list* queue;
};

/* FSM states for the boot0_ioctl API */
enum
{
    MODEM_STATE_OFFLINE,
    MODEM_STATE_DEFUNCT,
    MODEM_STATE_RESETTING,
    MODEM_STATE_RESET,
    MODEM_STATE_UNLOCKING_MEMORY,
    MODEM_STATE_UNLOCKED_MEMORY, /* FW upload allowed */
    MODEM_STATE_LOCKING_MEMORY,
    MODEM_STATE_LOCKED_MEMORY,
    MODEM_STATE_TURNING_ON,
    MODEM_STATE_TURNED_ON,
    MODEM_STATE_BOOTING_ON,
    MODEM_STATE_BOOTED_ON,
    MODEM_STATE_DL_STARTING,
    MODEM_STATE_DL_STARTED, /* IPC allowed */
    MODEM_STATE_BOOTING_OFF,
    MODEM_STATE_BOOTED_OFF, /* IPC allowed */
};

struct sipc_device
{
    /* instantiated once at device creation, do not need locking */
    size_t firmware_size;
    void __iomem* mcu_regs;
    void __iomem* firmware_mapping;
    void __iomem* ipc_mapping;
    struct miscdevice umts_ipc0;
    struct miscdevice umts_rfs0;
    struct miscdevice umts_boot0;
    struct net_device* rmnet0;
    struct regmap* pmu;
    struct clk* clk;
    int irq;
    /* mutexes for direct shmem access */
    struct mutex raw_tx_mtx;
    struct mutex fmt_tx_mtx;
    struct mutex irq_mtx;
    struct mutex fw_mtx;
    /* atomic values*/
    atomic_t had_cmd8;
    atomic_t modem_state;
    /* thread-safe values */
    struct wait_queue_head irq_waitqueue;
    /* values with embedded mutexes*/
    struct packet_list boot0_rx;
    struct packet_list ipc0_rx;
    struct packet_list rfs0_rx;
};
